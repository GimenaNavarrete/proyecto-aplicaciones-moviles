package com.example.franciscooviedo.appdenuncias.activity;

import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.franciscooviedo.appdenuncias.R;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.Complaint;

public class DetalleDenuncia extends AppCompatActivity {


    private ImageView ivFotoPerfil;
    private TextView tvNombre;
    private TextView tvFecha;
    private TextView tvLugar;
    private ImageView ivDenuncia;
    private TextView tvTitulo;
    private TextView tvDescripcion;
    private Button btnMensajes;
    private Button btnMas;
    private Button btnMenos;
    private Button btnBookmark;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_denuncia);

        ivFotoPerfil = (ImageView)findViewById(R.id.ivImagenes);
        tvNombre = (TextView)findViewById(R.id.tvNombre1);
        tvFecha = (TextView)findViewById(R.id.tvFecha);
        tvLugar = (TextView)findViewById(R.id.tvLugar);
        tvDescripcion = (TextView)findViewById(R.id.tvDescripcion);
        btnMensajes = (Button)findViewById(R.id.btnMensajes);
        btnMas = (Button)findViewById(R.id.btnMas);
        btnMenos = (Button)findViewById(R.id.btnMenos);
        btnBookmark = (Button)findViewById(R.id.btnBookmark);

    }


    public void setData(){
        Complaint complaintTemp = new Complaint();
        tvFecha.setText(complaintTemp.getCreated_at());
        tvLugar.setText(complaintTemp.getPlace());
        tvDescripcion.setText(complaintTemp.getDescription());

    }
}
