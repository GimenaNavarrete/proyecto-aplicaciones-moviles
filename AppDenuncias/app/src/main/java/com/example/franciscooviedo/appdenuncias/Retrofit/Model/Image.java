package com.example.franciscooviedo.appdenuncias.Retrofit.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by RodrigoR on 26/11/17.
 */

public class Image {
    @SerializedName("id")
    private int id;

    @SerializedName("id_complaint")
    private int id_complaint;

    @SerializedName("image")
    private String image;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_complaint() {
        return id_complaint;
    }

    public void setId_complaint(int id_complaint) {
        this.id_complaint = id_complaint;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
