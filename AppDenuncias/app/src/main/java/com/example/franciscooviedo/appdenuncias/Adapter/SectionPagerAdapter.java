package com.example.franciscooviedo.appdenuncias.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.franciscooviedo.appdenuncias.Fragment.FragmentPerfil;
import com.example.franciscooviedo.appdenuncias.Fragment.FragmentPost;
import com.example.franciscooviedo.appdenuncias.Recycler.Denuncias.data;
import com.example.franciscooviedo.appdenuncias.Retrofit.ApiDenuncias;

/**
 * Created by RodrigoR on 21/11/17.
 */

public class SectionPagerAdapter extends FragmentPagerAdapter {
    private FragmentPost fragmentsPosts = new FragmentPost();
    public SectionPagerAdapter(FragmentManager fm){
        super(fm);
    }

    public void cargarDatosPosts(){
        ApiDenuncias temp = new ApiDenuncias(this.fragmentsPosts);
        temp.ObtenerReclamos();
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return fragmentsPosts;
            case 1:
                return new FragmentPerfil();


            default: return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

}
