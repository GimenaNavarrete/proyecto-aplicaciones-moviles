package com.example.franciscooviedo.appdenuncias.activity.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.franciscooviedo.appdenuncias.Fragment.FragmentPost;
import com.example.franciscooviedo.appdenuncias.R;
import com.example.franciscooviedo.appdenuncias.Retrofit.ApiDenuncias;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.ApiActivity;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.User;

import java.util.ArrayList;
import java.util.List;

public class CrearCuentaActivity extends AppCompatActivity implements ApiActivity{
    private EditText et_nombre;
    private EditText et_apellido;
    private EditText et_usuarioc;
    private EditText et_telefono;
    private EditText et_correo;
    private EditText et_contra;
    private Button bt_registrarse;
    private CheckBox cb_anonimo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        //getSupportActionBar().hide();
        setContentView(R.layout.activity_crear_cuenta);

        et_nombre =(EditText) findViewById(R.id.et_nombre);
        et_apellido=(EditText) findViewById(R.id.et_apellido);
        et_telefono =(EditText) findViewById(R.id.et_telefono);
        et_correo =(EditText) findViewById(R.id.et_correo);
        et_contra =(EditText) findViewById(R.id.et_password);
        bt_registrarse =(Button) findViewById(R.id.bt_registrarse);
        cb_anonimo = (CheckBox) findViewById(R.id.cb_anonimo);

        final ApiActivity apiActivity=this;
        bt_registrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApiDenuncias apiDenuncias = new ApiDenuncias(apiActivity);
                User tempUser = new User();
                tempUser.setFirstname(et_nombre.getText().toString());
                tempUser.setLastname(et_apellido.getText().toString());
                tempUser.setPhone(et_telefono.getText().toString());
                tempUser.setEmail(et_correo.getText().toString());
                if(cb_anonimo.isChecked()){ tempUser.setAnonimo(1); } else { tempUser.setAnonimo(0); }
                tempUser.setPassword(et_contra.getText().toString());
                apiDenuncias.CrearUsuario(tempUser);
            }
        });
    }

    @Override
    public void setData(List listaRespuesta) {
        ArrayList<User> temp = (ArrayList<User>) listaRespuesta;
        if(temp.get(0).getId()!=0){
            Intent i = new Intent(this, IngresarActivity.class);
            startActivity(i);
        }
    }
}
