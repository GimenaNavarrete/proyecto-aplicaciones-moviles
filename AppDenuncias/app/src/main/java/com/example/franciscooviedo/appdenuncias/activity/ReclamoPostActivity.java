package com.example.franciscooviedo.appdenuncias.activity;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.example.franciscooviedo.appdenuncias.Adapter.SectionPagerAdapter;
import com.example.franciscooviedo.appdenuncias.R;

public class ReclamoPostActivity extends AppCompatActivity implements View.OnClickListener{

    private FloatingActionButton fab_Principal, fab_Secundario;
    private Toolbar toolbar;
    private ViewPager viewPager;
    private TabLayout tablayout;
    private SectionPagerAdapter sectionPagerAdapter;
    private boolean bandera_botonPrincipal=true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reclamo_post);

        /*List<Complaint> temp = ApiDenuncias.getInstance().ObtenerReclamos();
        for(Complaint temporal : temp){
            Log.i("Nombre: ", temporal.getDescription()); //Solo es para probar que si devuelve datos el web api
        }*/



        fab_Principal = (FloatingActionButton) findViewById(R.id.fab_Principal);
        fab_Secundario = (FloatingActionButton) findViewById(R.id.fab_secundario);
        fab_Principal.setOnClickListener(this);
        fab_Secundario.setOnClickListener(this);


        toolbar = (Toolbar) findViewById(R.id.toolbar_post);
        viewPager = (ViewPager) findViewById(R.id.pager_post);
        tablayout = (TabLayout) findViewById(R.id.tl_menu);

        setSupportActionBar(toolbar);
       // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sectionPagerAdapter = new SectionPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(sectionPagerAdapter);

        tablayout.setupWithViewPager(viewPager);
       // tablayout.getTabAt(0).setIcon(R.drawable.tl_feed_green);
       // tablayout.getTabAt(1).setIcon(R.drawable.tl_user_gray);

        //cargamos los datos base
        sectionPagerAdapter.cargarDatosPosts();
        tablayout.getTabAt(0).setIcon(R.drawable.tl_feed_green);
        tablayout.getTabAt(1).setIcon(R.drawable.tl_user_gray);
        fab_Principal.setImageResource(R.drawable.plus_one);
        getSupportActionBar().setTitle("Novedades");
        bandera_botonPrincipal=true;

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch(position){
                    case 0:
                        sectionPagerAdapter.cargarDatosPosts();
                        tablayout.getTabAt(position).setIcon(R.drawable.tl_feed_green);
                        tablayout.getTabAt(1).setIcon(R.drawable.tl_user_gray);
                        fab_Principal.setImageResource(R.drawable.plus_one);
                        fab_Secundario.setY(fab_Secundario.getY()+250f);
                        getSupportActionBar().setTitle("Novedades");
                        bandera_botonPrincipal=true;
                        break;

                    case 1:
                        tablayout.getTabAt(position).setIcon(R.drawable.tl_user_green);
                        tablayout.getTabAt(0).setIcon(R.drawable.tl_feed_gray);
                        fab_Principal.setImageResource(R.drawable.bookmark);
                        fab_Secundario.setImageResource(R.drawable.tag);
                        getSupportActionBar().setTitle("Perfil");
                        fab_Secundario.setY(fab_Secundario.getY()-250f);
                        bandera_botonPrincipal=false;
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){

            case R.id.fab_Principal:{
                if(bandera_botonPrincipal==true){

                    Intent i=new Intent(this, CrearDenuncia.class);

                    startActivity(i);
                }
                else{
                    Toast.makeText(this, "Este mostrara bookmark", Toast.LENGTH_SHORT).show();
                }
                break;

            }

            case R.id.fab_secundario:{
                Toast.makeText(this, "Este mostrara los ruedita de settings", Toast.LENGTH_SHORT).show();
                break;
            }
        }

    }


}

