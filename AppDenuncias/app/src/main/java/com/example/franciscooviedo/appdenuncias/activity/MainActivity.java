package com.example.franciscooviedo.appdenuncias.activity;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.franciscooviedo.appdenuncias.activity.login.LoginActivity;

import static java.lang.reflect.Array.getInt;


public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences preferences = getSharedPreferences("ACCOUNT", Context.MODE_PRIVATE);
        int userid=preferences.getInt("USER_ID", 0);
        if(userid==0){
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, ReclamoPostActivity.class);
            startActivity(intent);
        }


        //Intent intent = new Intent(this,ReclamoPostActivity.class);
              //startActivity(intent);


    }

}
