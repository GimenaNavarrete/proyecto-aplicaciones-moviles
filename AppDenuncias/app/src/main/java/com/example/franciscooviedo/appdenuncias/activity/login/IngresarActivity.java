package com.example.franciscooviedo.appdenuncias.activity.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.franciscooviedo.appdenuncias.Fragment.FragmentPost;
import com.example.franciscooviedo.appdenuncias.R;
import com.example.franciscooviedo.appdenuncias.Retrofit.ApiDenuncias;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.ApiActivity;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.User;
import com.example.franciscooviedo.appdenuncias.activity.ReclamoPostActivity;

import java.util.ArrayList;
import java.util.List;

public class IngresarActivity extends AppCompatActivity implements ApiActivity{
    private EditText et_correo;
    private EditText et_contrasena;
    private Button bt_ingresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        //getSupportActionBar().hide();
        setContentView(R.layout.activity_ingresar);

        et_correo = (EditText) findViewById(R.id.et_correo);
        et_contrasena = (EditText) findViewById(R.id.et_contrasena);
        bt_ingresar = (Button) findViewById(R.id.bt_ingresar);

        final ApiActivity apiActivity=this;
        bt_ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApiDenuncias apiDenuncias = new ApiDenuncias(apiActivity);
                User tempUser = new User();
                tempUser.setPassword(et_contrasena.getText().toString());
                tempUser.setEmail(et_correo.getText().toString());
                apiDenuncias.loginUsuario(tempUser);
            }
        });
    }

    @Override
    public void setData(List listaRespuesta) {
        ArrayList<User> temp = (ArrayList<User>) listaRespuesta;
        if(temp.get(0).getId()!=0){
            SharedPreferences preferences = getSharedPreferences("ACCOUNT", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("USER_ID", temp.get(0).getId());
            editor.commit();

            Intent i = new Intent(this, ReclamoPostActivity.class);
            startActivity(i);
        } else {
            Toast.makeText(this, "Incorrecto", Toast.LENGTH_SHORT).show();
        }
    }
}
