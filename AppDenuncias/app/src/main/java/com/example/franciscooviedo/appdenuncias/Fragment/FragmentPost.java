package com.example.franciscooviedo.appdenuncias.Fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franciscooviedo.appdenuncias.R;
import com.example.franciscooviedo.appdenuncias.Recycler.Denuncias.adapter.DenunciaAdapter;
import com.example.franciscooviedo.appdenuncias.Recycler.Denuncias.data;
import com.example.franciscooviedo.appdenuncias.Recycler.Denuncias.interfaces.ItemClickListener;
import com.example.franciscooviedo.appdenuncias.Retrofit.ApiDenuncias;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.ApiActivity;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.Complaint;
import com.example.franciscooviedo.appdenuncias.activity.DetalleDenuncia;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RodrigoR on 21/11/17.
 */

public class FragmentPost extends android.support.v4.app.Fragment implements ApiActivity{
    View rootView;
    RecyclerView rvDenuncias;
    SwipeRefreshLayout srl_post;
    private DenunciaAdapter mDenunciaAdapter;
    private List<Complaint> mItemList = new ArrayList<>();
    private TextView tvNombre;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        rootView = inflater.inflate(R.layout.fragment_post, container, false);
        rvDenuncias = (RecyclerView) rootView.findViewById(R.id.rvDenuncias);
        srl_post = (SwipeRefreshLayout) rootView.findViewById(R.id.srl_post);
        mDenunciaAdapter = new DenunciaAdapter(mItemList, new ItemClickListener<Complaint>() {
            @Override
            public void OnClick(Complaint item) {
                Intent intent =new Intent(rootView.getContext(), DetalleDenuncia.class);
                intent.putExtra("ComplaintId", item.getId());
                startActivity(intent);
            }
        });



        rvDenuncias.setHasFixedSize(true);
        rvDenuncias.setAdapter(mDenunciaAdapter);
        LinearLayoutManager mLLCatalogData = new LinearLayoutManager(getActivity());
        mLLCatalogData.setOrientation(LinearLayoutManager.VERTICAL);
        rvDenuncias.setLayoutManager(mLLCatalogData);

        srl_post.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });


        return rootView;

    }
    public void refreshItems() {
        ApiDenuncias temp = new ApiDenuncias(this);
        temp.ObtenerReclamos();
        srl_post.setRefreshing(false);
    }


    @Override
    public void setData(List listaRespuesta) {
        mItemList.clear();
        mItemList.addAll(listaRespuesta);
        mDenunciaAdapter.notifyDataSetChanged();
    }


}
