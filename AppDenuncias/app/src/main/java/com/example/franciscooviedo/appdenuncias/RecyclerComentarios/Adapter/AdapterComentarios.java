package com.example.franciscooviedo.appdenuncias.RecyclerComentarios.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.franciscooviedo.appdenuncias.R;
import com.example.franciscooviedo.appdenuncias.Recycler.Denuncias.ViewHolder.DenunciaViewHolder;
import com.example.franciscooviedo.appdenuncias.Recycler.Denuncias.interfaces.ItemClickListener;
import com.example.franciscooviedo.appdenuncias.RecyclerComentarios.ViewHolder.ComentarioViewHolder;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.Comment;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.Complaint;

import java.util.List;

/**
 * Created by Gimena Navarrete on 25/11/2017.
 */

public class AdapterComentarios extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Comment> mListItems;
    private ItemClickListener<Comment> mClickListener;


    public AdapterComentarios(List<Comment> items, ItemClickListener<Comment> clickListener){
        mListItems = items;
        mClickListener = clickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_comentario_view_holder,parent,false);
        return new ComentarioViewHolder(v, mClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ComentarioViewHolder viewHolder = (ComentarioViewHolder) holder;
        viewHolder.setItem(mListItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }
}
