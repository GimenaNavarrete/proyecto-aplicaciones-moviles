package com.example.franciscooviedo.appdenuncias.RecyclerComentarios.Actividades;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.franciscooviedo.appdenuncias.R;
import com.example.franciscooviedo.appdenuncias.Recycler.Denuncias.adapter.DenunciaAdapter;
import com.example.franciscooviedo.appdenuncias.Recycler.Denuncias.interfaces.ItemClickListener;
import com.example.franciscooviedo.appdenuncias.RecyclerComentarios.Adapter.AdapterComentarios;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.ApiActivity;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.Comment;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.Complaint;

import java.util.ArrayList;
import java.util.List;

public class Comentarios extends AppCompatActivity implements ApiActivity {

    RecyclerView rvComentarios;
    private AdapterComentarios mAdapterComentarios;
    private List<Comment> mItemList = new ArrayList<>();
    private Activity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comentarios);

        mActivity = this;
        mItemList = new ArrayList<>();
        rvComentarios = (RecyclerView) findViewById(R.id.rvComentarios);
        mAdapterComentarios = new AdapterComentarios (mItemList, new ItemClickListener<Comment>() {
            @Override
            public void OnClick(Comment item) {

            }
        });


        rvComentarios.setAdapter(mAdapterComentarios);
        rvComentarios.setHasFixedSize(true);
        LinearLayoutManager mLLCatalogData = new LinearLayoutManager(this);
        mLLCatalogData.setOrientation(LinearLayoutManager.VERTICAL);
        rvComentarios.setLayoutManager(mLLCatalogData);
        mItemList.clear();
        //mItemList.addAll(clasesData.getData());
        mAdapterComentarios.notifyDataSetChanged();
    }

    @Override
    public void setData(List listaRespuesta) {

    }
}
