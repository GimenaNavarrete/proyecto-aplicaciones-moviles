package com.example.franciscooviedo.appdenuncias.Retrofit.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by RodrigoR on 26/11/17.
 */

public class TagComplaint {
    @SerializedName("id")
    private int id;

    @SerializedName("id_tag")
    private int id_tag;

    @SerializedName("id_complaint")
    private int id_complaint;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("updated_at")
    private String updated_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_tag() {
        return id_tag;
    }

    public void setId_tag(int id_tag) {
        this.id_tag = id_tag;
    }

    public int getId_complaint() {
        return id_complaint;
    }

    public void setId_complaint(int id_complaint) {
        this.id_complaint = id_complaint;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
