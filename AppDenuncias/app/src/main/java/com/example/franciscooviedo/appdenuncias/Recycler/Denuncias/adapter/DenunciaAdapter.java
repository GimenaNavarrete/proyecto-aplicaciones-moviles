package com.example.franciscooviedo.appdenuncias.Recycler.Denuncias.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.franciscooviedo.appdenuncias.R;
import com.example.franciscooviedo.appdenuncias.Recycler.Denuncias.ViewHolder.DenunciaViewHolder;
import com.example.franciscooviedo.appdenuncias.Recycler.Denuncias.interfaces.ItemClickListener;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.Complaint;

import java.util.List;

/**
 * Created by Francisco Oviedo on 24 nov 2017.
 */

public class DenunciaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Complaint> mListItems;
    private ItemClickListener<Complaint> mClickListener;


    public DenunciaAdapter(List<Complaint> items, ItemClickListener<Complaint> clickListener){
        mListItems = items;
        mClickListener = clickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post,parent,false);
        return new DenunciaViewHolder(v, mClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        DenunciaViewHolder viewHolder = (DenunciaViewHolder) holder;
        viewHolder.setItem(mListItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }
}