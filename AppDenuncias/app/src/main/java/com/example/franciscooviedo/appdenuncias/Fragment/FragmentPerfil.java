package com.example.franciscooviedo.appdenuncias.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.franciscooviedo.appdenuncias.R;

/**
 * Created by RodrigoR on 21/11/17.
 */

public class FragmentPerfil extends android.support.v4.app.Fragment {

    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        rootView= inflater.inflate(R.layout.fragment_perfil,container,false);

        return rootView;

    }
}
