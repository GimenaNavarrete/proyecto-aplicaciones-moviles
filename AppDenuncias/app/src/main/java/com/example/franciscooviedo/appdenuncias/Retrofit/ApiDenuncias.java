package com.example.franciscooviedo.appdenuncias.Retrofit;

import android.util.Log;
import android.view.View;

import com.example.franciscooviedo.appdenuncias.Retrofit.Model.ApiActivity;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.Complaint;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Francisco Oviedo on 24 nov 2017.
 */

public class ApiDenuncias {
    private static ApiDenuncias mApiInstance = null;
    private static Retrofit mRetrofit = null;
    private ApiActivity mActivity;

    public ApiDenuncias(ApiActivity apiActivity){
        mRetrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.43.86:8089/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.mActivity = apiActivity;
    }

    public void ObtenerUsuarios(){
        DenunciasApi service = mRetrofit.create(DenunciasApi.class);
        Call<List<User>> users = service.listUsers();
        users.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                List<User> userList = response.body();
                List listaRespuesta = new ArrayList<User>();
                for (User temp : userList) {
                    listaRespuesta.add(temp);
                }
                mActivity.setData(listaRespuesta);
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.e("Error", t.getMessage());
            }
        });
    }

    public void ObtenerReclamos(){
        DenunciasApi service = mRetrofit.create(DenunciasApi.class);
        Call<List<Complaint>> complaints = service.listComplaints();
        complaints.enqueue(new Callback<List<Complaint>>() {
            @Override
            public void onResponse(Call<List<Complaint>> call, Response<List<Complaint>> response) {
                List<Complaint> complaintList = response.body();
                List listaRespuesta = new ArrayList<Complaint>();

                for (Complaint temp : complaintList) {
                    listaRespuesta.add(temp);
                }
                mActivity.setData(listaRespuesta);
            }

            @Override
            public void onFailure(Call<List<Complaint>> call, Throwable t) {
                Log.e("Error", t.getMessage());
            }
        });
    }

    public void CrearUsuario(User user){
        DenunciasApi service = mRetrofit.create(DenunciasApi.class);
        Call<User> usercreate = service.createUser(user);
        usercreate.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User user = response.body();
                Log.d("EL ID DEL USUARIO", String.valueOf(user.getId()));
                List listaRespuesta = new ArrayList<User>();
                listaRespuesta.add(user);
                mActivity.setData(listaRespuesta);
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e("Error", t.getMessage());
            }
        });
    }

    public void loginUsuario(User user){
        DenunciasApi service = mRetrofit.create(DenunciasApi.class);
        Call<User> userlogin = service.loginUser(user);
        userlogin.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User user = response.body();
                List listaRespuesta = new ArrayList<User>();
                listaRespuesta.add(user);
                mActivity.setData(listaRespuesta);
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e("Error", t.getMessage());
            }
        });
    }

    public void CrearDenuncia(Complaint complaint){
        DenunciasApi service = mRetrofit.create(DenunciasApi.class);
        Call<Complaint> newComplaint = service.createComplaint(complaint);
        newComplaint.enqueue(new Callback<Complaint>() {
            @Override
            public void onResponse(Call<Complaint> call, Response<Complaint> response) {
                Complaint responseComplaint = response.body();
                List listaRespuesta = new ArrayList<Complaint>();
                listaRespuesta.add(responseComplaint);
                mActivity.setData(listaRespuesta);
            }

            @Override
            public void onFailure(Call<Complaint> call, Throwable t) {
                Log.e("Error", t.getMessage());
            }
        });
    }
}
