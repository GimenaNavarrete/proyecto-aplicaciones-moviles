package com.example.franciscooviedo.appdenuncias.Retrofit;

import com.example.franciscooviedo.appdenuncias.Retrofit.Model.Complaint;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Francisco Oviedo on 24 nov 2017.
 */

public interface DenunciasApi {
    @GET("users")
    Call<List<User>> listUsers();

    @GET("complaints")
    Call<List<Complaint>> listComplaints();

    @POST("users/create")
    Call<User> createUser(@Body User user);

    @POST("users/login")
    Call<User> loginUser(@Body User user);

    @POST("complaints/create")
    Call<Complaint> createComplaint(@Body Complaint complaint);
}
