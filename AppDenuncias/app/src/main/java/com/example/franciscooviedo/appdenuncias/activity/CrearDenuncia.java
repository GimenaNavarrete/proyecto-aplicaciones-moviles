package com.example.franciscooviedo.appdenuncias.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.franciscooviedo.appdenuncias.R;
import com.example.franciscooviedo.appdenuncias.Retrofit.ApiDenuncias;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.ApiActivity;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.Complaint;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.User;

import java.util.List;

public class CrearDenuncia extends AppCompatActivity implements ApiActivity{

    private static final int PICK_IMAGE = 100;
    Button bt_camara;
    Button bt_galeria;
    private RadioButton btAnomalia;
    private RadioButton btincidente;
    private RadioButton btincidenteMayor;
    private EditText et_titulo;
    private EditText et_comentario;
    private Button boton_guardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.crear_denuncia);

        btAnomalia = (RadioButton) findViewById(R.id.btAnomalia);
        btincidente = (RadioButton) findViewById(R.id.btincidente);
        btincidenteMayor = (RadioButton) findViewById(R.id.btincidente);
        et_titulo = (EditText) findViewById(R.id.et_titulo);
        et_comentario = (EditText) findViewById(R.id.et_comentario);
        boton_guardar = (Button) findViewById(R.id.boton_guardar);

        bt_galeria=(Button) findViewById(R.id.btgaleria);
        bt_galeria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        final ApiActivity apiActivity=this;
        boton_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApiDenuncias apiDenuncias = new ApiDenuncias(apiActivity);
                Complaint tempComplaint = new Complaint();
                int severity=0;
                if(btAnomalia.isChecked()){
                    severity=1;
                }else if(btincidente.isChecked()){
                    severity=2;
                }else if(btincidenteMayor.isChecked()){
                    severity=3;
                }
                tempComplaint.setSeverity(severity);
                tempComplaint.setTitle(et_titulo.getText().toString());
                tempComplaint.setDescription(et_comentario.getText().toString());
                tempComplaint.setPlace("");
                SharedPreferences preferences = getSharedPreferences("ACCOUNT", Context.MODE_PRIVATE);
                int userid=preferences.getInt("USER_ID", 0);
                tempComplaint.setId_user(userid);
                apiDenuncias.CrearDenuncia(tempComplaint);
            }
        });

    }

    private void openGallery(){
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode == RESULT_OK && requestCode == PICK_IMAGE){

        }
    }

    @Override
    public void setData(List listaRespuesta) {
        Toast.makeText(this, "Se creo su denuncia", Toast.LENGTH_SHORT).show();
    }
}
