package com.example.franciscooviedo.appdenuncias.Retrofit.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Gimena Navarrete on 25/11/2017.
 */

public class Comment {
    @SerializedName("id")
    private int id;

    @SerializedName("id_user")
    private int id_user;

    @SerializedName("id_complaint")
    private int id_complaint;

    @SerializedName("comment")
    private String comment;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("updated_at")
    private String updated_at;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_complaint() {
        return id_complaint;
    }

    public void setId_complaint(int id_complaint) {
        this.id_complaint = id_complaint;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

}
