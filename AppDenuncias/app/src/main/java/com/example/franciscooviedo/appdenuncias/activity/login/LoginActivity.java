package com.example.franciscooviedo.appdenuncias.activity.login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.example.franciscooviedo.appdenuncias.R;

public class LoginActivity extends AppCompatActivity {
    private Button bnt_ingresar;
    private Button bnt_CrearCuenta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        //getSupportActionBar().hide();
        setContentView(R.layout.activity_login);

        bnt_ingresar = (Button) findViewById(R.id.bt_ingresar);
        bnt_CrearCuenta = (Button) findViewById(R.id.bt_crearCuenta);
    }

    public void mostrarIngresar (View view){
        Intent i=new Intent(this,IngresarActivity.class);
        startActivity(i);
    }

    public void mostrarCrearCuenta (View view){
        Intent i=new Intent(this,CrearCuentaActivity.class);
        startActivity(i);
    }
}
