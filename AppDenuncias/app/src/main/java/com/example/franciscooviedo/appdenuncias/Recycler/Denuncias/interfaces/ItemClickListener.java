package com.example.franciscooviedo.appdenuncias.Recycler.Denuncias.interfaces;

/**
 * Created by Francisco Oviedo on 24 nov 2017.
 */

public interface ItemClickListener <T> {
    void OnClick(T item);
}