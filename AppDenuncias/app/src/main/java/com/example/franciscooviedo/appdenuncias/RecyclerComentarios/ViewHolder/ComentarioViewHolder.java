package com.example.franciscooviedo.appdenuncias.RecyclerComentarios.ViewHolder;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.franciscooviedo.appdenuncias.R;
import com.example.franciscooviedo.appdenuncias.Recycler.Denuncias.interfaces.ItemClickListener;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.Comment;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.Complaint;

public class ComentarioViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private Comment mItem;
        private Context mContext;

        private ConstraintLayout clHeader;
        private ImageView ivFotoPerfil;
        private TextView tvNombre;
        private TextView tvComentario;

        private ItemClickListener<Comment> mClickListener;

    public ComentarioViewHolder(View v, ItemClickListener<Comment> clickListener) {
        super(v);
        mContext = itemView.getContext();
        itemView.setOnClickListener(this);
        mClickListener = clickListener;

        clHeader = (ConstraintLayout) itemView.findViewById(R.id.clHeader);
        ivFotoPerfil = (ImageView) itemView.findViewById(R.id.civ_perfil);
        tvNombre = (TextView) itemView.findViewById(R.id.tvNombre);
        tvComentario = (TextView) itemView.findViewById(R.id.tvComentario);
    }

    public void setItem(Comment item){
        mItem = item;
    }

    @Override
    public void onClick(View v) {
        if(mClickListener != null){
            mClickListener.OnClick(mItem);
        }
    }
}


