package com.example.franciscooviedo.appdenuncias.Recycler.Denuncias.ViewHolder;

import android.content.Context;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.franciscooviedo.appdenuncias.R;
import com.example.franciscooviedo.appdenuncias.Recycler.Denuncias.interfaces.ItemClickListener;
import com.example.franciscooviedo.appdenuncias.Retrofit.Model.Complaint;

/**
 * Created by Francisco Oviedo on 24 nov 2017.
 */

public class DenunciaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private Complaint mItem;
    private Context mContext;

    private ConstraintLayout clHeader;
    private ImageView ivFotoPerfil;
    private TextView tvNombre;
    private TextView tvFecha;
    private Button btnOpciones;
    private TextView tvLugar;
    private ImageView ivDenuncia;
    private TextView tvTitulo;
    private TextView tvDescripcion;
    private Button btnMensajes;
    private Button btnMas;
    private Button btnMenos;
    private Button btnBookmark;

    private ItemClickListener<Complaint> mClickListener;

    public DenunciaViewHolder(View itemView, ItemClickListener<Complaint> clickListener) {
        super(itemView);
        mContext = itemView.getContext();
        itemView.setOnClickListener(this);
        mClickListener = clickListener;

        clHeader = (ConstraintLayout) itemView.findViewById(R.id.clHeader);
        ivFotoPerfil = (ImageView) itemView.findViewById(R.id.civ_perfil);
        tvNombre = (TextView) itemView.findViewById(R.id.tvNombre);
        tvFecha = (TextView) itemView.findViewById(R.id.tvFecha);
        btnOpciones = (Button) itemView.findViewById(R.id.btnOpciones);
        tvLugar = (TextView) itemView.findViewById(R.id.tvLugar);
        ivDenuncia = (ImageView) itemView.findViewById(R.id.ivDenuncia);
        tvTitulo = (TextView) itemView.findViewById(R.id.tvTitulo);
        tvDescripcion = (TextView) itemView.findViewById(R.id.tvDescripcion);
        btnMensajes = (Button) itemView.findViewById(R.id.btnMensajes);
        btnMas = (Button) itemView.findViewById(R.id.btnMas);
        btnMenos = (Button) itemView.findViewById(R.id.btnMenos);
        btnBookmark = (Button) itemView.findViewById(R.id.btnBookmark);

    }

    public void setItem(Complaint item){
        mItem = item;

        switch(item.getSeverity()){
            case 1: clHeader.setBackgroundColor(ContextCompat.getColor(mContext, R.color.Anomalia)); break;
            case 2: clHeader.setBackgroundColor(ContextCompat.getColor(mContext, R.color.Incedente)); break;
            case 3: clHeader.setBackgroundColor(ContextCompat.getColor(mContext, R.color.IncedenteMayor)); break;
            case 4: clHeader.setBackgroundColor(ContextCompat.getColor(mContext, R.color.IncedenteSerio)); break;
        }


        if(!(item.getCreated_at()==null)){tvFecha.setText(item.getCreated_at().toString());};
        tvLugar.setText(item.getPlace().toString());
        tvTitulo.setText(item.getTitle().toString());
        tvDescripcion.setText(item.getDescription().toString());

        btnMas.setText(String.valueOf(item.getRanking_plus()));
        btnMenos.setText(String.valueOf(item.getRanking_minus()));

        tvNombre.setText(item.getUser().getFirstname()+ " "+item.getUser().getLastname());
    }

    @Override
    public void onClick(View v) {
        if(mClickListener != null){
            mClickListener.OnClick(mItem);
        }
    }
}

